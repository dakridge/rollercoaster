Array.prototype.sortByKey = function(key)
{
	return this.sort(function(a, b)
	{
		var x = a[key]; var y = b[key];
		return ((x < y) ? -1 : ((x > y) ? 1 : 0));
	});
};

$.fn.hasAttr = function(name) { return this.attr(name) !== undefined; };

function Rollercoaster()
{
	var that = this;

	var options = {
		"curtain"			: false,
		"default-position"	: "top"
	};

	this.launch = launch;
	this.setOptions = setOptions;
	this.getOptions = getOptions;

	var checkpoints = $("[rollercoaster]").map(function(i, d){ return { "element": d, "order": $(d).attr("rollercoaster") }; });
	checkpoints = checkpoints.toArray();
	checkpoints.sortByKey("order");

	function launch()
	{
		if( checkpoints.length == 0 ) //crash the rollercoaster if there are no checkpoints
			return;

		var iCount = 0;

		if( options.curtain )
			var curtain = $("<div>").attr("class", "rollercoaster-bg").appendTo("body").fadeIn(600);

		$(document).on("keyup", function(e)
		{
			if( e.which == 39 || e.which == 13 )
				move();
		});

		nextCar();

		function nextCar()
		{
			var a = checkpoints[iCount];
			iCount++;

			var el = $(a.element);
			var position = (( el.hasAttr("rollercoaster-position") ) ? el.attr("rollercoaster-position") : options["default-position"] );
			var dim = { "w": el.outerWidth(), "h": el.outerHeight() };
			var offset = el.offset();

			if( !inWindow(el) )
				$('body,html').animate({ 'scrollTop': offset.top - 100 }, 0);

			var m = 10; //margin
			var pad = 20;

			var car = createCar(el);
			car.appendTo( "body" )
				.fadeIn(500);

			if( options.curtain )
			{
				el.addClass("rollercoaster-showme");

				$("<div>").attr("class", "rollercoaster-outline")
					.css(
					{
						"width"		: dim.w + pad,
						"height"	: dim.h + pad,
						"left"		: offset.left - 1 - (pad/2),
						"top"		: offset.top - 1 - (pad/2)
					})
					.appendTo( "body" );
			}

			if( position == "right" )
			{
				car.css(
				{
					"left"	: ( options.curtain ) ? offset.left + dim.w + m + (pad/2) : offset.left + dim.w + m,
					"top"	: offset.top + ( dim.h / 2 ) - ( car.outerHeight() / 2 )
				});
			}
			else if( position == "left" )
			{
				car.css(
				{
					"left"	: ( options.curtain ) ? offset.left - car.outerWidth() - m - (pad/2) : offset.left - car.outerWidth() - m,
					"top"	: offset.top + ( dim.h / 2 ) - ( car.outerHeight() / 2 )
				});
			}
			else if( position == "top" )
			{
				car.css(
				{
					"left"	: offset.left - (car.outerWidth() / 2) + ( dim.w / 2 ),
					"top"	: ( options.curtain ) ? offset.top - car.outerHeight() - m - (pad/2) : offset.top - car.outerHeight() - m
				});
			}
			else if( position == "bottom" )
			{
				car.css(
				{
					"left"	: offset.left - (car.outerWidth() / 2) + ( dim.w / 2 ),
					"top"	: ( options.curtain ) ? offset.top + dim.h + m + (pad/2) : offset.top + dim.h + m
				});
			}

			car.on("click", function(){ move(); });
		}

		function move()
		{
			$(".rollercoaster-car").remove();

			if( options.curtain )
			{
				$(".rollercoaster-showme").removeClass( "rollercoaster-showme" );
				$(".rollercoaster-outline").remove();
			}

			if( checkpoints.length > iCount )
				nextCar();
			else
			{
				//we have reached the end of the ride, turn off listeners
				$(document).off("keyup");

				if( options.curtain )
					curtain.fadeOut(400, function(){ curtain.remove(); });
			}
		}
	}

	function createCar(el)
	{
		var html = (( el.hasAttr("rollercoaster-content") ) ? el.attr("rollercoaster-content") : "Wassup");
		var next = $("<div>").attr("class", "next")
			.append( $("<i>").attr("class", "icon-chevron-sign-right") );

		return $("<div>").attr("class", "rollercoaster-car")
			.html( html )
			.append( next );
	}

	function inWindow(el)
	{
		//determines if the element, el, exists in the current window

		var _y = window.pageYOffset;
		var _y0 = $(window).outerHeight() + _y;
		var p = el.offset();

		if( _y < p.top && _y0 > p.top )
			return true;
		else
			return false;
	}

	function setOptions(o)
	{
		options.curtain = set("curtain");

		function set(s)
		{
			return ( o.hasOwnProperty(s) ) ? o[s] : options[s];
		}
	}

	function getOptions(){ return options; }

}