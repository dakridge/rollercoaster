rollercoaster.js
================

An easy website tour / tutorial maker

How to Use
----------

Each DOM element that you want to be involved in the rollercoaster needs the following attributes:

- rollercoaster : holds the index value for the firing order
- rollercoaster-content : the content for the tooltip. This can include HTML.
- rollercoaster-position : the position of the tooltip [top, right, bottom, left]
 

Here is an example of the html


    <div rollercoaster="2" rollercoaster-content="Here is a tooltip message" rollercoaster-position="top"></div>
 
 
 The rollercoaster can be initiated like so:

    var coaster = new Rollercoaster();
    coaster.setOptions(
    {
        "curtain"   : true
    });
    coaster.launch();
